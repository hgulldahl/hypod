package imagemetadata

import (
	"crypto"
	"fmt"
	"github.com/gosexy/checksum"
	"github.com/gosexy/exif"
	"os"
	"path/filepath"
	"regexp"
	"time"
	//"github.com/gosexy/sugar"
	"github.com/gosexy/to"
)

type MetaData struct {
	shutter     float64
	aperture    float64
	iso         int64
	orientation int64
	flashed     bool
	path        string
	hash        string
	model       string
	date        time.Time
}

func getMetaData(file string) (data MetaData, err error) {
	re := regexp.MustCompile(`(\d{4}):(\d{2}):(\d{2}) (\d{2}):(\d{2}):(\d{2})`)

	ex := exif.New()
	err = ex.Open(file)

	if err == nil {
		data.path, _ = filepath.Abs(file)
		data.hash = checksum.File(file, crypto.SHA1)
		tags := ex.Tags
		data.model = to.String(tags["Model"])
		data.aperture = to.Float64(tags["FNumber"])
		data.iso = to.Int64(tags["ISOSpeedRatings"])
		data.iso = to.Int64(tags["Orientation"])
		data.shutter = to.Float64(tags["ExposureTime"])
		data.flashed = to.Bool(tags["Flash"])

		var taken string

		dateFields := []string{
			"Date and Time (Original)",
			"Date/Time Original",
			"Media Create Date",
			"Track Create Date",
			"Create Date",
		}

		for _, field := range dateFields {
			if tags[field] != "" {
				taken = tags[field]
				break
			}
		}

		if taken == "" {
			// have to use file timestamp. ouch!
			filestat, _ := os.Stat(file)
			data.date = filestat.ModTime()
			return
		}

		all := re.FindAllStringSubmatch(taken, -1)

		data.date = time.Date(
			to.Int(all[0][1]),
			time.Month(to.Int(all[0][2])),
			to.Int(all[0][3]),
			to.Int(all[0][4]),
			to.Int(all[0][5]),
			to.Int(all[0][6]),
			0,
			time.UTC,
		)
	}
	return

}
